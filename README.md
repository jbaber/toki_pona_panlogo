# toki_pona_panlogo

An attempt to get a short list of sentences containing all words in [Toki Pona](https://en.wikipedia.org/wiki/Toki_Pona)

a, akesi ala li alasa e ali. -> Ah, no reptile gathers everything.

anpa li ante e esun. -> The floor changes the market.

ijo ike li awen insa e jan jaki. -> The evil object rests in the guts of the disgusting man.

kala li jo e ilo jelo. -> The fish has a yellow tool.

kalama li kama e kasi. -> The sound summoned the plant.

kulupu ken kepeken e kiwen en e ko, kin. -> The village can use stone and sap, as well.

kili lape li kute e kon kule. -> The sleeping fruit listens to the colorful wind.

len lili lete li laso. -> The small cold clothing is blue.

akesi linja loje li lon e lawa ona insa e lupa pi jan lipu. -> The red snake puts its head in the contortionist's orifice.

mama meli mi li lukin e luka pi mama mije mi. -> My mom looks at my dad's arm.

mun li moku e mani lipu namako mute. -> The moon ate the spiced dollar bills.

nena nasa li lon e pipi moli insa nasin seli. -> The crazy mountain puts the dead bug in the hot street.

noka mi li nasa. monsi ona li jo e oko mute! -> My leg is weird. It has many eyes on the back!

nasin tu wan. nasin ni li tawa ma musi. nasin ni li tawa ma pakala. nasin pi nanpa tu wan li tawa ma pimeja. -> There are three paths. This path leads to an entertaining country. That path leads to the ruined country. The third path leads to the dark country.

mi mute li pali e pan palisa. o, sina open e seli. mi lon e ko ni: insa seli. mi mute li moku e pan palisa. -> We'll make a baguette. You turn on the oven. I'll put the dough in the oven. Then we'll eat the baguette.

pipi ala li jo e olin. -> Bugs don't have hearts.

Not included yet: - anu, la, mu, nimi, pana, pilin, selo, seme, sewi, telo, tomo, unpa, uta, waso

